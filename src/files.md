# Files

## `ls-files`
```bash
# List files in Git.
git ls-files
git ls-files -c
```

```bash
# List modified files.
git ls-files -m
```

```bash
# List other (untracked, ignored, ...) files.
git ls-files -o
git ls-files --others

# List directories other files are in.
git ls-files -o --directory
```

```bash
# List untracked ignored files, based upon .gitignore.
git ls-files -io --exclude-from=.gitignore
```

## `cat-file`
```bash
# Pretty-print the contents of HEAD.
git cat-file -p HEAD
```

```bash
# Pretty-print the contents of a tree ID (file list), for example the tree ID from -p HEAD.
git cat-file -p <id_from_tree>
```

```bash
# Pretty-print the contents of an ID, for example the parent ID from -p HEAD.
git cat-file -p <id_from_parent>
```
