# Fixing commits
```bash
# Update the last commit's message.
git commit --amend -m "Message"

# Update the last commit's date (reflected on GitHub).
git commit --amend --no-edit --date="Fri Nov 6 20:00:00 2016 -0600"

# Add another file to the last commit. Uses the last message.
git add <file>
git commit --amend -C HEAD

# Revert the last commit.
git revert HEAD

# Revert only the second to last commit. Etcetera
git revert HEAD~1

# Revert the last three commits, but stage the reversion locally.
git revert --no-commit HEAD~3..

# Reset working files to match master (or another branch), removing local changes and commits.
git fetch --all
git reset --hard origin/<branchName>
```
