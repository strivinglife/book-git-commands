# Updating Gitea
See https://docs.gitea.io/en-us/install-from-binary/#updating-to-a-new-version for more information.

The following covers what I've done for my instance.

## Create a backup
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
5. `exit`

## Update 1.16.8 to 1.16.9
1. `VERSION=1.16.9`
2. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
3. `sudo systemctl status gitea`
	- Stop if needed/didn't backup.
4. `sudo mv gitea /usr/local/bin/gitea`
5. `sudo chmod +x /usr/local/bin/gitea`
6. `sudo systemctl restart gitea`
7. `sudo systemctl status gitea`
8. After some period of time, delete the backup file.

## Updating 1.16.9 to 1.17.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
5. `exit`
6. `VERSION=1.17.1`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
13. After some period of time, delete the backup file.

## Updating 1.17.1 to 1.17.2
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
5. `exit`
6. `VERSION=1.17.2`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.17.2 to 1.17.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
5. `exit`
6. `VERSION=1.17.3`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

Gitea LFS config also updated:

1. `sudo vim /etc/gitea/app.ini`
2. Move server.LFS_CONTENT_PATH to a new lfs.PATH (`/var/lib/gitea/data/lfs`).
3. `sudo systemctl restart gitea`
4. `sudo systemctl status gitea`

## Updating 1.17.3 to 1.17.4
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.17.4`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.17.4 to 1.18.0
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.18.0`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.18.0 to 1.18.3
Updates came pretty fast, so I ran all three of these one after the other.
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.18.1`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. Test the site.
14. `sudo systemctl stop gitea`
15. `VERSION=1.18.2`
16. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
17. `sudo mv gitea /usr/local/bin/gitea`
18. `sudo chmod +x /usr/local/bin/gitea`
19. `sudo systemctl restart gitea`
20. Test.
21. `sudo systemctl stop gitea`
22. `VERSION=1.18.3`
23. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
24. `sudo mv gitea /usr/local/bin/gitea`
25. `sudo chmod +x /usr/local/bin/gitea`
26. `sudo systemctl restart gitea`
27. Test.
28. After some period of time, delete the backup file.

## Updating 1.18.3 to 1.18.5
1.18.4 included a bug.
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.18.5`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.18.5 to 1.19.0
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.19.0`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

### 1.19.0 Indexer Updates

> 2023/03/22 21:46:34 ...s/setting/setting.go:614:deprecatedSetting() [E] Deprecated fallback `[indexer]` `UPDATE_BUFFER_LEN` present. Use `[queue.issue_indexer]` `LENGTH` instead. This fallback will be removed in v1.19.0

1. Remove `UPDATE_BUFFER_LEN` from `[indexer]`.
2. If not the default of 20 (https://docs.gitea.io/en-us/config-cheat-sheet/#queue-queue-and-queue), follow the message to set the correct length property.
	- In my case I had the default of 20, so I dropped it entirely.

## Updating 1.19.0 to 1.19.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.19.1`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.19.1 to 1.19.2
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.19.2`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.19.2 to 1.19.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.19.3`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.19.3 to 1.19.4
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.19.4`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.19.4 to 1.20.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.20.0`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. Test the site.
14. `sudo systemctl stop gitea`
15. `VERSION=1.20.1`
16. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
17. `sudo systemctl status gitea`
	- Should be inactive/dead.
18. `sudo mv gitea /usr/local/bin/gitea`
19. `sudo chmod +x /usr/local/bin/gitea`
20. `sudo systemctl restart gitea`
21. `sudo systemctl status gitea`
	- Should be active (running).
22. After some period of time, delete the backup file.

## Updating 1.20.1 to 1.20.2
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.20.2`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- Should be active (running).
13. After some period of time, delete the backup file.
14. `sudo vim /etc/gitea/app.ini`
15. Remove `[log].ROUTER = console` option as it's currently the default.
	- This setting has otherwise moved to a new location.
16. `sudo systemctl restart gitea`

## Updating 1.20.2 to 1.20.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.20.3`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea` then `q`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea` then `q`
	- Should be active (running).
13. After some period of time, delete the backup file.
14. Verify that `[storage].PATH` is not set via `cat /etc/gitea/app.ini`.

## Updating 1.20.3 to 1.20.4
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.20.4`
7. `sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
8. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
9. `sudo mv gitea /usr/local/bin/gitea`
10. `sudo chmod +x /usr/local/bin/gitea`
11. `sudo systemctl restart gitea`
12. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
13. After some period of time, delete the backup file.

## Updating 1.20.4 to 1.20.5
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.20.5 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea`
9. `sudo chmod +x /usr/local/bin/gitea`
10. `sudo systemctl restart gitea`
11. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
12. After some period of time, delete the backup file.

## Updating 1.20.5 to 1.21.0
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.0 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea`
9. `sudo chmod +x /usr/local/bin/gitea`
10. `sudo systemctl restart gitea`
11. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
12. After some period of time, delete the backup file.

## Updating 1.21.0 to 1.21.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.1 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea`
9. `sudo chmod +x /usr/local/bin/gitea`
10. `sudo systemctl restart gitea`
11. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
12. After some period of time, delete the backup file.

## Updating 1.21.1 to 1.21.2
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.2 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea`
9. `sudo chmod +x /usr/local/bin/gitea`
10. `sudo systemctl restart gitea`
11. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
12. After some period of time, delete the backup file.

## Updating 1.21.2 to 1.21.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.3 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.3 to 1.21.4
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.4 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.4 to 1.21.5
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.5 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl status gitea`
	- then `q`
	- Should be inactive/dead.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl status gitea`
	- then `q`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.5 to 1.21.6
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.6 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.6 to 1.21.7
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.7 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.7 to 1.21.10
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.10 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.10 to 1.21.11
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.21.11 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.21.11 to 1.22.0
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.0 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.0 to 1.22.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.1 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.1 to 1.22.2
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.2 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.2 to 1.22.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.3 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.3 to 1.22.4
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.4 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.4 to 1.22.6
Version 1.22.5 was quickly replaced.

1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.22.6 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. After some period of time, delete the backup file.

## Updating 1.22.6 to 1.23.1
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.23.0 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. `sudo systemctl stop gitea`
12. `VERSION=1.23.1 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
13. `sudo systemctl is-active gitea`
	- Should be inactive.
14. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
15. `sudo systemctl restart gitea`
16. `sudo systemctl is-active gitea`
	- Should be active (running).
17. After some period of time, delete the backup file.
## Updating 1.23.1 to 1.23.3
1. `sudo systemctl stop gitea`
2. `sudo su - git`
3. `gitea dump -c /etc/gitea/app.ini -w /var/lib/gitea -t /tmp`
4. `ls -l --block-size=M`
	- `rm <old-backup-file>`
	- `chmod a=r <new-backup-file>`
5. `exit`
6. `VERSION=1.23.2 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
7. `sudo systemctl is-active gitea`
	- Should be inactive.
8. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
9. `sudo systemctl restart gitea`
10. `sudo systemctl is-active gitea`
	- Should be active (running).
11. `sudo systemctl stop gitea`
12. `VERSION=1.23.3 && sudo wget -O gitea https://dl.gitea.io/gitea/${VERSION}/gitea-${VERSION}-linux-amd64`
13. `sudo systemctl is-active gitea`
	- Should be inactive.
14. `sudo mv gitea /usr/local/bin/gitea && sudo chmod +x /usr/local/bin/gitea`
15. `sudo systemctl restart gitea`
16. `sudo systemctl is-active gitea`
	- Should be active (running).
17. After some period of time, delete the backup file.
