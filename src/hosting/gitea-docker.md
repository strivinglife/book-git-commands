# Hosting Gitea in Docker
This is specific to a local/internal instance, running in Docker (24.0.5) on Ubuntu (22.04), with Docker Compose (2.20.2).

Latest Gitea at the time of writing is 1.20.1.

> Warning: This likely isn't the most secure.

1. Create a new user on the server for Git.
	- `sudo adduser --system --shell /bin/bash --gecos 'Git Version Control' --group --disabled-password --home /home/git git`
	- Make note of the UID and GID's returned.
2. `mkdir gitea && cd gitea`
3. `nano docker-compose.yml`
	- Populate it with the contents below, setting the correct `USER_UID` and `USER_GID` values.
4. Start it up.
	- `docker compose up -d`
5. Navigate to http://server-ip:port and create an account to verify.

To update Gitea config, you can update the local file. Assuming docker-compose.yml was saved to ~/docker/gitea:

`sudo vim docker/gitea/gitea/gitea/conf/app.ini`

The following may be beneficial to add:

- `repository` > `ENABLE_PUSH_CREATE_USER = true`
- `repository` > `ENABLE_PUSH_CREATE_ORG = true`

## docker-compose.yml
> Replace 117 and 122 UID and GID with the appropriate values. Replace `8070` and `2227` with the desired ports.

```yaml
version: "3"

networks:
  gitea:
    external: false

services:
  server:
    image: gitea/gitea:1.20.1
    container_name: gitea
    environment:
      - USER_UID=117
      - USER_GID=122
    restart: always
    networks:
      - gitea
    volumes:
      - ./gitea:/data
      - /etc/timezone:/etc/timezone:ro
      - /etc/localtime:/etc/localtime:ro
    ports:
      - "8070:3000"
      - "2227:22"
```
