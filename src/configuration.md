# Configuration
The following sections covers general configuration and information.

* [User information](configuration/user.md)
* [Git configuration](configuration/git.md)
* [Repository information](configuration/repository.md)
* [Beyond Compare configuration](configuration/beyond-compare.md)
