# Notes and warnings
If you want to escape, typically <kbd>Shift</kbd> + <kbd>Q</kbd> is the way to do so on Windows. This includes after running a command like `git log` (depending upon what it uses; <kbd>Q</kbd> will exit if it's using less for paging).

Also on Windows, remember that you can use `notepad <filename>` to open/create a file in Notepad.