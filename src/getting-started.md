# Getting started
```bash
# Clone a repository into a new directory in the current directory.
git clone _.git
git clone _.git differentFolderName

# Create a new repository in the current directory.
git init
```

```bash
# Add a new remote for a repo. In this case 'upstream' might be helpful for the repo this was forked from.
git remote add upstream _.git

# Create a new branch.
git branch <branchName>

# Switch current repo to a different, existing, branch.
git checkout <branchName>
git checkout master
```

## Clone troubleshooting
```bash
# If running into issues cloning a repository, clones a shallow copy. Then updates remotes and fetches everything.
git clone --depth=1 _.git
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch --unshallow
```
