# Reporting
```bash
# Show all current branches, including remotes.
git show-branch -a --list

# Show all local branches that have been merged into master.
git branch --merged

# Show all local branches that have not been merged into master.
git branch --no-merged

# Show all local branches, sorted by and showing last commit
# https://stackoverflow.com/a/5188364/11912
git for-each-ref --sort=committerdate refs/heads/ --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(color:red)%(objectname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
```
