# Rebasing
[git rebase](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase) in the Atlassian Git Tutorial has a nice overview of this command.

```bash
# Rebase the current branch by appying them to the passed branch.
git rebase <branchName>

# Continue a rebase.
git rebase --continue
```
