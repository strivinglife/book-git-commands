# Worktree

```bash
# List all worktrees for the current repo.
git worktree list

# Create a new worktree for a specific branch. Path can't be a current working directory.
git worktree add ../path/for/branchName

# Delete a worktree.
git worktree remove path/to/worktree
```
