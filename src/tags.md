# Tags
```bash
# List all tags.
git tag

# Create a new tag, locally.
git tag -a TagName -m "Message"

# Delete a tag.
git tag -d TagName

# Push a tag to a remote (local by default).
git push origin TagName
```
