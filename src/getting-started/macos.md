# Getting started on macOS

Use [Homebrew](https://brew.sh/).

## Install Git
```bash
brew install git
```

## Update Git
If you haven't updated brew in a while, you can both make sure it and all applications are current.
```bash
brew update && brew upgrade
```

Otherwise just upgrade Git itself:
```bash
brew upgrade git
```
