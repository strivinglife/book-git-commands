# Stashing changes
```bash
# Stash changes in tracked files.
git stash

# Also stash untracked changes.
git stash -u

# List all stashes.
git stash list

# View all stashes in a pretty list.
git stash list --pretty=format:'%Cblue%gd%Cred: %C(yellow)%s'

# View changes in all stashes.
git stash list -p
# Pipe into VS Code.
git stash list -p | code -

# Show file changes in a particular stash (0 = last one).
git stash show 'stash@{0}'

# Show individual changes in a particular stash.
git stash show -p 'stash@{0}'

# Show stashed changes in individual file.
git diff ..stash -- <file>

# Create a patch file from the above.
git diff ..stash -- <file> > name.patch

# Apply stashed changes in an individual file.
git diff ..stash -- <file> | git apply

# Apply changes from the last stash. Keep the stash.
git stash apply
git stash apply 'stash@{0}'

# Apply changes from the last stash. Drop the stash after.
# git stash pop = git stash apply && git stash drop
git stash pop

# Drop the most recent stash.
git stash drop
git stash drop 'stash@{0}'
git stash drop 0

# Drop a particular stash.
git stash drop 'stash@{2}'
```

## Using a temporary stash to pull/merge
```bash
# If you can't pull/merge due to a file conflict, create a temporary stash.
git stash
# Pull changes.
git pull
# Apply and drop the stash.
git stash pop
```
