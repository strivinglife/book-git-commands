# Branching
```bash
# Create a new branch. Stay in the current branch.
git branch <branchName>

# Create a new branch at a specific commit, and switch to it.
git checkout -b <branchName> 3f4308df1e0b7f663d851108128e8e53deb1b5fb

# Switch to an existing branch.
git checkout <branchName>
git checkout master

# Switch to the previous branch.
git switch -
git switch @{-1}
# For use in PowerShell.
git switch '@{-1}'
git checkout -
git checkout @{-1}

# Checkout and switch to a new branch.
git checkout -b my_new_branch
git switch -c my_new_branch

# Push the new branch to a remote.
git push -u origin my_new_branch

# Alternative way to push a branch to a remote, without permanently setting the upstream.
git push origin my_new_branch

# Push to a remote, but only up to a certain commit. This helps if the full push is too large.
git push origin c5831166a7189823919923c73a0e6346cbee71d6:main
# If the branch hasn't been created on the remote yet.
git push origin c5831166a7189823919923c73a0e6346cbee71d6:refs/heads/main

# Checkout a remote branch.
git checkout --track origin/<branchName>

# Merge changes from master into a branch.
git checkout <branchName>
git merge master

# Abort a merge (such as if there's conflicts that need to be resolved differently).
git merge --abort

# Merge two local branches, without fast-forwarding, with default editor opening to modify the message.
git merge <branchName> --no-ff --edit

# Merge two local branches, without fast-forwarding, and including messages for the last X (20 here) commits.
# According to Linus Torvalds, the better way to do merges.
git merge <branchName> --log=20 --no-ff

# While on a different branch, update the master branch from remote.
# See https://stackoverflow.com/a/42902058/11912
git fetch origin master:master

# Put recent local commits in master into a new branch.
# X is the number of commits to rollback.
git branch <branchName>
git reset --hard HEAD~X
git checkout <branchName>

# Add a specific commit on top of the current branch.
git cherry-pick <commit-id>

# Add a specific commit on top of the current branch, but don't automatically commit it. Allows you to manually tweak what's getting pulled in.
git cherry-pick -n <commitId>

# Rename the current branch
git branch -m <newBranchName>

# Show all local branches with last commit message.
git branch -v

# Above, plus corresponding remote branch name. Also includes worktree path, if applicable.
git branch -vv
```

## Navigating
```bash
# Get the name of the previous branch.
git rev-parse --symbolic-full-name @{-1}
# For PowerShell.
git rev-parse --symbolic-full-name '@{-1}'
```

## Management
```bash
# Delete a branch.
git branch -d my_new_branch

# Delete a branch on the remote.
git push origin :my_new_branch

# Do a dry run of pruning local branches that no longer exist on a remote.
git remote prune origin --dry-run
```
