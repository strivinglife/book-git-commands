# Summary

* [Introduction](introduction.md)
* [Notes and warnings](notes.md)
* [Getting Started](getting-started.md)
	* [macOS](getting-started/macos.md)
	* [Windows](getting-started/windows.md)
* [Configuration](configuration.md)
	* [User information](configuration/user.md)
	* [Git configuration](configuration/git.md)
	* [Repository information](configuration/repository.md)
	* [.gitattributes](configuration/gitattributes.md)
	* [Beyond Compare configuration](configuration/beyond-compare.md)
* [Included Tools](tools.md)
* [Commits](commits.md)
	* [Fixing commits](commits/fixing.md)
* [Stashing](stashing.md)
* [Collaboration](collaboration.md)
* [Branching](branching.md)
	* [Rebasing](branching/rebasing.md)
	* [Reporting](branching/reporting.md)
	* [Worktree](branching/worktree.md)
* [Tags](tags.md)
* [Logging](logging.md)
	* [Output examples](logging/outputs.md)
* [Files](files.md)

## Examples
* [Updating a fork](example/update-fork.md)
* [Create a new repo from a subdirectory](example/create-new-repo-from-subdirectory.md)
* [Aliases](example/aliases.md)

## Self-hosting
* [Gitea](hosting/gitea.md)
	* [Updating Gitea](hosting/updating-gitea.md)
	* [Hosting in Docker](hosting/gitea-docker.md)

## Related
* [Related links](related.md)
