# Commits

```bash
# Use a basic GUI. Actually works quite well for staging hunks that can't be split.
git gui
```

## Viewing changes
```bash
# See what has changed/etcetera
git status

# See only changes to tracked files
git status -uno
git status --untracked-files=no

# See which files have changed.
git diff --stat

# See how many files have changed (insertions/deletions). Linux/macOS.
git diff --stat | tail -n1

# See what or how many files are staged.
git diff --cached --stat
git diff --cached --stat | tail -n1

# See differences, with words colored inline.
git diff --color-words

# Difference. Use Shift + Q to quit. (Q may also work.)
git diff <file>

# Difference, ignoring space changes (EOL and multiple into one).
git diff -b <file>

# See what changed in a file that's already been staged.
git diff --cached <file>
```

## Staging changes
```bash
# Add/stage a new/updated file.
git add <file>

# Add/stage multiple files, space delimited
git add <file> <file>

# Add a file with prompts on what to do with hunks
git add <file> -p
git add <file> --patch

# Add/stage all changes with prompts on what to do with hunks.
git add -p

# Add/stage all changes (including deletions)
git add -u

# Work in Interactive mode.
git add -i

# Add/stage file deletion.
git rm <file>

# Add/stage file move/rename (such as case sensitive change)
git mv -f <file> <File>

# Add/stage directory rename.
git mv <oldDirectoryName> <newDirectoryName>
```

## Reverting local changes
```bash
# Unstage change.
git reset HEAD <file>
git restore --staged <file>

# Unstage all changes.
git reset

# Selectively unstage changes to files.
git reset -p

# Hard reset the current branch back one commit.
git reset --hard HEAD^

# Discard changes to a file
git checkout -- <file>

# Selectively discard changes to a file
git checkout -p

# Discard all local changes to tracked files. Leaves new files/folders as-is. Helpful if you did a mass find/replace and want to undo it.
git checkout -f
git checkout --force

# Discard all untracked files, interactively.
git clean -i

# Get a file from a particular commit.
git checkout a1b2c3 -- <file>

# Get a file from the commit previous to the commit. Helpful if you want to revert a change just made to a file.
git checkout a1b2c3~1 -- <file>
```

## Commit changes
```bash
# Commit with Message.
git commit -m "Message"

# Commit with a summary and detail. Additional -m parameters can be passed as needed.
git commit -m "Summary" -m "Details."
git commit -m "Summary" -m "Details." -m "Another line/paragraph of details."

# Add all changed files and commit. New files are not committed.
git commit -am "Message"

# Commit with a date override.
git commit -m "Initial back-dated commit" --date="20050703T07:18"
# View the overridden author date and actual commit date.
git show --format=fuller
```

## More commit viewing
```bash
# Show changes made in a particular commit.
git show <commit_id>

# Show changes made in last commit.
git show HEAD

# Show the message and files from a particular commit.
git show --stat <commit_id>

# See a list of changes made in branchName2 not in branchName1.
git diff <branchName1>...<branchName2>

# See a list of files that have changed in the current branch, compared to master. Includes number of files and inserts/deletes.
git diff --stat master...
git diff --stat master...<branchName>

# See the number of changed files, and how many inserts/deletes there were in a branch, since master.
git diff --shortstat master...<branchName>

# See a list of just the file names that were changed in a branch, since master.
git diff --name-status master...<branchName>
```
