# Aliases
The following are some aliases that I have setup.

Find them by running the following
```bash
git config --get-regexp 'alias.*'
```

## User and Repository Related
```bash
# Define an alias to change user information for a repo.
git config --global alias.workprofile 'config user.email "work@example.com"'
# With the above alias defined, run in a repo directory to switch user information for the specific repo.
git workprofile
```

## Logging
```bash
git config --global alias.last 'log -1 HEAD --stat'
git config --global alias.lg "!git lg1"
git config --global alias.lg1 "!git lg1-specific --all"
git config --global alias.lg2 "!git lg2-specific --all"
git config --global alias.lg3 "!git lg3-specific --all"
git config --global alias.lg1-specific "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'"
git config --global alias.lg2-specific "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)'"
git config --global alias.lg3-specific "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset)%n''          %C(dim white)- %an <%ae> %C(reset) %C(dim white)(committer: %cn <%ce>)%C(reset)'"
```

## Remotes
```bash
git config --global alias.incoming "!git remote update -p; git log ..@{u}"
git config --global alias.outgoing "log @{u}.."
```
