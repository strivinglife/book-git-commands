# Example: Updating a fork

```bash
# On the fork:
git checkout master

git pull upstream master

git push origin master

# If the branch should be created.
git checkout -b new_branch
git push -u origin new_branch

# If the branch already exists.
git checkout branch_name
git merge master

# If there are conflicts:
# Uses the default merge tool against all conflicts, one at a time.
git mergetool
# Uses the default merge commit message.
git commit --no-edit
```
