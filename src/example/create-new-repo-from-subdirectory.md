# Example: Create a new repo from a subdirectory
Useful if you want to pull a directory, and its commit history, out into a new repo.

```bash
# Clone the repository into a new directory.
git clone _.git

# Get only the files and commits impacting a particular directory.
# This should be the name of the directory only; if using PowerShell or the like make sure it does not include path information.
# <branchName> is the name of the default branch, generally master.
git filter-branch --prune-empty --subdirectory-filter <pathToDirectory> <branchName>

# View and then update your remotes with the new repo location.
git remote -v
git remote set-url origin _.git

git push -u origin <branchName>
```
