# Logging
```bash
# View the commit log, in various ways.
git log
git log --oneline
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'
git log --graph --date=short --pretty=format:'%C(yellow)%h%C(reset) %C(green)%ad%C(reset) %C(red)|%C(reset) %s %C(bold blue)[%an]%C(reset)%C(yellow)%d%C(reset)'
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen%cn%Creset %Cblue(%cr)%Creset' --abbrev-commit --date=relative
git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
git log --graph --pretty=format:'%C(auto)%h%Creset - %d%s %Cgreen(%cr) %C(bold magenta)<%an>%Creset'
git log --decorate --graph --abbrev-commit --date=relative
git log --graph --pretty=format:'%C(yellow)%h%C(cyan)%d%Creset %s %C(white)- %an, %ar%Creset'

# View the last commit's message and changed files.
git log -1 HEAD --stat

# View commits that touched a file.
git log --full-history -- <file>

# View commits that touched a directory.
git log --full-history -- *\<directory-name>\*

# View commits that touched a directory.
git log <directory-path>
git log --pretty=oneline <directory-path>
git log --stat <directory-path>

# View last X commits, with message and files, that touched a directory.
git log --stat -X <directory-path>
git log --name-status -X .

# View commits that touched a file.
git log -p <file>

# View commits that touched a file, including renames.
git log --follow -p <file>

# View commits that have deleted files.
git log --diff-filter=D --summary

# View commits that changed a file, even if deleted.
git log --all -- <file>

# View all deleted files.
git log --all --pretty=format: --name-only --diff-filter=D | sort -u

# Search commits messages for specific text (case sensitive).
git log --grep="searchTerm"

# Search commit diffs for changes in count of text (added or removed).
git log -SsearchTerm
git log -SsearchTerm -i

# Search commit diffs for specific text (case sensitive) showing changed lines.
git log -SsearchTerm -p

# Search commit diffs for changes involving search term.
# You can type /searchTerm to use the pager to find the first instance, and then n to find the next one(s).
git log -GsearchTerm -p

# Search commit contents for specific text.
git grep "searchTerm"
git grep -i "searchTerm"

# View all commits that are in branch-2 that are not in branch-1.
git log branch-1..branch-2

# View all commits ending with a particular branch
git log <branchName> --graph
git log <branchName> --graph --oneline

# View all commits merging a branch into master
git log --merges --first-parent --format=oneline

# View where all branches are in the commit history.
git log --color --graph --oneline --decorate --simplify-by-decoration --all

# View all users who committed to the repository, ordered by number of commits
git shortlog -s -n

# View mostly commonly modified files, based upon commits.
git log --pretty=format: --name-only | sort | uniq -c | sort -rg | head -10

# View all files in Git with the date the file was last touched in Git.

# macOs.
git ls-files | while read file; do git log -n 1 --pretty="Filename: $file, commit: %h, date: %ad" -- $file; done | less

# PowerShell.
git ls-tree -r --name-only HEAD | ForEach-Object { "$(git log -1 --format="%ai" -- "$_")`t$_" }
```
