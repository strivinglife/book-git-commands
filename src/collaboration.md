# Collaboration
```bash
# Pull from default remote.
git pull

# Push to default remote.
git push

# Push to 'origin' remote from 'master' branch.
git push origin master

# Sync with the repo this was forked from / the remote associated with 'upstream.'
git pull upstream master

# Fetch from all remotes, by default, unless remotes are setup in the config.
git remote update
```
