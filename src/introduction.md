# Introduction
The following is a dump of Git commands for use on your shell of choice.

This book is [generated with mdBook][mdbook] and hosted by [GitLab Pages](https://gitlab.com/strivinglife/book-git-commands).

The book can be served locally by running `mdbook serve`.

## History
In mid-2014 I started [a GitHub Gist](https://gist.github.com/JamesSkemp/15fcf0147cb85bb633bf) with the commands to work with Git (having moved from TFS and Subversion before that). As that Gist grew it became rather unwieldly, so I looked for something better, where I could continue to create Markdown files with the various commands that I found helpful as I learned Git.

In 2018 GitBook was a great choice, but in 2019 they changed their business model and mdBook was there to seamlessly fill the gap. Thankfully GitLab Pages continues to be a great resource for hosting this material.

Even if no one else uses it, I regularly find a need to come back to, and expand, this book, as there's just some commands that I need to look up. But I hope I'm not the only one that reads these words and gets some benefit from them.

Comments and suggestions welcome, especially if you have some cool logging formats you'd like to share. Git repo linked above and on the top right of each page.

[mdbook]: https://github.com/rust-lang/mdBook
