# Included Tools

```bash
# Simple GUI for Git.
git gui
# Same, but detached.
git-gui
```

```bash
# Simple GUI showing current branch commits.
gitk
# Simple GUI showing all branches and commits.
gitk --all
```
