# Git configuration
```bash
# See where configuration options are set.
git config --list --show-origin
git config --list --show-origin --name-only
git config --list --show-origin --show-scope --name-only

# Edit system/global/local.
git config --system --edit
```

## Defaults
```bash
# Set the default branch name when using git init. Here set to 'main'.
git config --global init.defaultBranch main
```
