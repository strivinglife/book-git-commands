# User information
```bash
# Pull user information.
git config user.name
git config user.email

# Update all repository user name.
git config --global user.name "Your Name"

# Update individual repository user name / email.
git config user.name "Your Name"
git config user.email "your_email@example.com"

# Store credentials in cache/memory (default 900 seconds/15 minutes).
git config credential.helper cache
```
