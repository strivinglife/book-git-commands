# Repository information

## Configuration
```bash
# List config details, including remotes, for the current repo.
git config --local -l

# List all configuration settings for the current repository, including global and system.
git config -l
```

## Remotes
```bash
# Change remote origin URL (repo name change or move).
git remote set-url origin _.git

# See what remotes are setup on a repo.
git remote -v

# See more information about remotes (origin in this case).
git remote show origin

# Add a new remote with name _ and location _.git.
git remote add _ _.git

# Rename a remote.
git remote rename old-name new-name

# Delete a named remote.
git remote rm _

# Delete the origins push URL.
# May be necessary for projects that explicitly set a fetch and push URL.
git remote set-url --delete --push origin push.git
```

## Branches
```bash
# List all local branches.
git branch --list

# List all branches, including remotes.
git branch -a

# List all branches merged into master (for cleanup).
git branch --merged
```

## Size
```bash
# List database information, include size (size-pack = kb).
git count-objects -v

# Runs database clean-up.
git gc

# Check validity of objects in the database.
# File System Check
git fsck
```
