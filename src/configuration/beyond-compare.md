# Beyond Compare
If you use [Beyond Compare](https://www.scootersoftware.com/), the following sets up Git to use it for diff and merge.

```bash
git config --global diff.tool bc
git config --global difftool.bc.path "c:/Program Files/Beyond Compare 4/bcomp.exe"
git config --global merge.tool bc
git config --global mergetool.bc.path "c:/Program Files/Beyond Compare 4/bcomp.exe"
# This is up to the user on whether they want to keep .orig files.
git config --global mergetool.keepBackup false
```
