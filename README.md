# Introduction
The following is a dump of Git commands for use on your shell of choice.

This book is [generated with mdBook][mdbook] and hosted by [GitLab Pages](https://gitlab.com/strivinglife/book-git-commands).

The book can be served locally by running `mdbook serve`.

[mdbook]: https://github.com/rust-lang/mdBook
